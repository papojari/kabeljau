# kabeljau 🐱

[![asciicast of the kabeljau start screen](https://asciinema.org/a/487970.png)](https://asciinema.org/a/487970)

In this game you play as a stray cat. You goal is to survive until the end.

You interact via an ncurses tui.

## Dependencies

### Buildtime

- [Just](https://just.systems/)
- ImageMagick

### Runtime

- [dialog](https://invisible-island.net/dialog/dialog.html)

## Packages

| Operating System                                     | Package Manager  | Package                     | Command                                                                           |
| ---------------------------------------------------- | ---------------- | --------------------------- | --------------------------------------------------------------------------------- |
| [Arch Linux][arch linux]                             | [pacman][pacman] | [kabeljau][kabeljau-pacman] | `git clone https://aur.archlinux.org/kabeljau.git && cd kabeljau && makepkg -sri` |
| [NixOS][nixos], [Linux][nix-plat], [macOS][nix-plat] | [Nix][nix]       | [kabeljau][kabeljau-nixpkg] | `nix-env -iA nixos.kabeljau` or `nix-env -iA nixpkgs.kabeljau`                    |

[arch linux]: https://www.archlinux.org
[pacman]: https://wiki.archlinux.org/title/Pacman
[kabeljau-pacman]: https://aur.archlinux.org/packages/kabeljau
[nixos]: https://nixos.org/nixos/
[nix-plat]: https://nixos.org/nix/manual/#ch-supported-platforms
[nix]: https://nixos.org/nix/
[kabeljau-nixpkg]: https://github.com/NixOS/nixpkgs/blob/master/pkgs/tools/misc/kabeljau/default.nix

![package version table](https://repology.org/badge/vertical-allrepos/kabeljau.svg)

## License

©️ 2022 Anna Aurora Kitsüne <mailto:anna@annaaurora.eu> <https://annaaurora.eu>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.

