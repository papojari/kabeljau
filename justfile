#!/usr/bin/env just --justfile
name:='kabeljau'
build-path:='./build'
bin-path:='/usr/bin'
share-path:='/usr/share'

# By default, recipes are only listed.
default:
	@just --list

linux-build:
	#!/bin/sh
	set -euxo pipefail
	# Clear {{build-path}}
	mkdir -p {{build-path}}
	rm -rf {{build-path}}
	# Executable
	install -Dm700 {{name}}.bash {{build-path}}/usr/bin/{{name}}
	# .desktop
	install -Dm600 {{name}}.desktop -t {{build-path}}/usr/share/applications
	# Icons
	for icon_width in 32 64 128; do
		mkdir -p {{build-path}}/usr/share/icons/hicolor/$icon_width"x"$icon_width/apps
		out_path={{build-path}}/usr/share/icons/hicolor/$icon_width"x"$icon_width/apps/{{name}}.png
		if [ $icon_width = 32 ]; then
			cp icons/{{name}}.png $out_path
		else
			magick icons/{{name}}.png -scale $icon_width"x"$icon_width $out_path
		fi
	done

linux-install: linux-build
	#!/bin/sh
	set -euxo pipefail
	# Executable
	install -Dm755 {{build-path}}/usr/bin/{{name}} -t {{bin-path}}
	# .desktop
	install -Dm644 {{build-path}}/usr/share/applications/{{name}}.desktop -t {{share-path}}/applications
	# Icons
	for icon_width in 32 64 128; do
		install -Dm644 {{build-path}}/usr/share/icons/hicolor/$icon_width"x"$icon_width/apps/{{name}}.png -t {{share-path}}/icons/hicolor/$icon_width"x"$icon_width/apps
	done
	echo "Successfully installed {{name}}"

