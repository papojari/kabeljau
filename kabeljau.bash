#!/usr/bin/env bash
# This file is part of the kabeljau source code.
#
# ©️ 2022 papojari <mailto:papojari-git.ovoid@aleeas.com> <https://matrix.to/#/@papojari:artemislena.eu> <https://papojari.codeberg.page>
#
# For the license information, please view the README.md file that was distributed with this source code.

set -eu # So you don't have to keep using &&

begged_for_food() {
	ANSWER_0="Try to catch a rodent"
	ANSWER_1="Take a stroll through the neighbourhood"
	{ SELECTED_ANSWER=$(dialog --menu "After you meowed on some human's door for hours, they opened and put food and water for you in front of the door. You ate and drank it all and then fell asleep. In the night, you wake up." 12 54 12 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

	# If the 0th answer was chosen…
	if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
		ANSWER_0="Take a rest in the sun"
		ANSWER_1="Take a stroll through the neighbourhood"
		{ SELECTED_ANSWER=$(dialog --menu "You were successful in catching a mouse. After playing with it a bit for the fun you ate it." 11 48 11 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

		# If the 0th answer was chosen…
		if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
			ANSWER_0="Run away as fast as you can"
			ANSWER_1="Fight the dog"
			{ SELECTED_ANSWER=$(dialog --menu "You were disrupted in your sleep by a very aggressive dog running towards you and barking at you." 11 48 11 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

			# If the 0th answer was chosen…
			if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
				ANSWER_0="Try to catch a bird"
				ANSWER_1="Take a stroll through the neighbourhood"
				{ SELECTED_ANSWER=$(dialog --menu "You were too fast for the dog and escaped it." 9 48 9 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

				# If the 0th answer was chosen…
				if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
					tried_to_catch_bird
				else
					strolled_though_neighbourhood
				fi
			# If the 1th answer was chosen…
			else
				dialog --msgbox "You took serious injuries from the fight and died." 6 33
				ALIVE=false
			fi
		else
			strolled_though_neighbourhood
		fi
	# If the 1th answer was chosen…
	else
		strolled_though_neighbourhood
	fi
}

strolled_though_neighbourhood() {
	ANSWER_0="Investigate the house"
	ANSWER_1="Try to catch a bird"
	{ SELECTED_ANSWER=$(dialog --menu "While strolling through the neighbourhood you found a house where several other cats house." 10 50 10 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

	# If the 0th answer was chosen…
	if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
		ANSWER_0="Try to eat from the feeding bowls"
		ANSWER_1="Meow in front of the humans of the house."
		{ SELECTED_ANSWER=$(dialog --menu "You dabbled a bit into the front yard of the house and saw several feeding bowls." 10 48 10 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

		# If the 0th answer was chosen…
		if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
			dialog --msgbox "While you ate from the bowls the other cats saw you and scared you away." 7 33
			ALIVE=true
			STRAY=true
		else
			dialog --msgbox "The humans were kind and let you live at their house. After slowly approaching the other cats, you made friends with them." 8 40
			ALIVE=true
			STRAY=false
		fi
	# If the 1th answer was chosen…
	else
		tried_to_catch_bird
	fi
}

tried_to_catch_bird() {
	dialog --msgbox "While trying to catch a bird you were driven over by a car and died." 7 33
	ALIVE=false
}

# Start screen with explanation on how to play the game.
dialog --msgbox "This is a game where you play by deciding between several options. Depending on your answer, the story develops. You are a stray cat in a suburb. Survive until the end. Bonus points if you find a home at a human's place." 9 50

# Set possible answers for first question.
ANSWER_0="Try to catch a rodent"
ANSWER_1="Beg a human for food"

# Ask the first question.
{ SELECTED_ANSWER=$(dialog --menu "You haven't eaten for days and are hungry." 10 28 10 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

# If the 0th answer was chosen…
if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
	ANSWER_0="Try to find a water source to drink from"
	ANSWER_1="Beg a human for food"
	{ SELECTED_ANSWER=$(dialog --menu "You tried hard but the you didn't have the energy to go fast. Maybe try again when you have eaten something." 11 48 11 "$ANSWER_0" "" "$ANSWER_1" "" 2>&1 1>&$out); } {out}>&1

	# If the 0th answer was chosen…
	if [[ $SELECTED_ANSWER == $ANSWER_0 ]]; then
		dialog --msgbox "By walking around too much you lost all energy and died." 6 33
		ALIVE=false
	else
		begged_for_food
	fi
# If the 1th answer was chosen…
else
	begged_for_food
fi

# The end.
if [[ $ALIVE = true ]]; then
	dialog --msgbox "You made it to the end. Thanks for playing." 6 40
elif [[ $ALIVE = true ]] && [[ $STRAY = false ]]; then
	dialog --msgbox "You made it to the end and even found a home. Thanks for playing." 6 40
else
	dialog --msgbox "Sadly, you didn't make it to the end. You may want to play again." 6 36
fi

exit 0
